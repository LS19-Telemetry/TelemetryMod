TelemetryServer = {}; 
TelemetryServer.metadata = {
	interface = " FS19 ...",
	title = " TelemetryServer",
	notes = " Outputs Data to XML-File",
	author = " (by BurrrY)",
	version = " v0.1",	
	datum = " 27.11.2018",
	update = " 27.11.2018",
	web = " no"
};

local nOutputTicksCurrent = 0
local dataFile = g_modsDirectory.."/TelemetryData.xml";

function TelemetryServer:loadMap(name)
	g_currentMission.TelemetryServer = TelemetryServer;	
	print("---loading".. tostring(TelemetryServer.metadata.title).. tostring(TelemetryServer.metadata.version).. tostring(TelemetryServer.metadata.author).. "---")
	if g_server ~= nil and g_client ~= nil and g_dedicatedServerInfo ~= nil then return;end;
	TelemetryServer.settingsDir = getUserProfileAppPath().. "modsSettings/TelemetryServer/";																					
	TelemetryServer.simple = {isOn=true,rebootXml=false,showHelpOn=false,showHelpOff=true,allTxtBold=false,vehicleSchemaPosY=0,defaultPosX=0.01,helpHudPosY=0.98,defaultPosY=0.98,defaultSize=0.010,diffPosX=0.001,diffPosY=0.005,
	showField=false,showSpeed=true,showFillName=true,realFillName=false,isFillFull=false,showFillLevel=true,showTrain=false,showProzent=true,ownFarmVehicle=false,showPlayerName=true,showVehicleName=false,showVehicleSmallName=true;isHelper=true,isPlayer=true,isBlocking=true,isFuel=false,isWater=true,
	showVehicleTypeName=false,isDamage=false,showLastAllAttached=true, attachedLimit=4, seperator="|"};
	if g_currentMission.hud ~= nil and g_currentMission.hud.vehicleSchema ~= nil and g_currentMission.hud.vehicleSchema.overlay ~= nil then TelemetryServer.simple.vehicleSchemaPosY = Utils.getNoNil(g_currentMission.hud.vehicleSchema.overlay.y, TelemetryServer.simple.defaultPosY);end;
	TelemetryServer.simple.txt = {		
		speed = " kmh";
		field = "F ";
		fillNameToFillLevel = " ";  --is Seperator
		helperS = "H";
		otherT = "O";				
		playerS = "P";				
		otherSaFull = "! Empty !";
		otherSa = "^";				--is Seperator, show better Seeds/sowing etc.
		otherSaOrFill = "?";		--is Seperator, Seeds/sowing or Fill is Unknown etc.
		otherW = "! Water !";
		otherD = "! Damage !";
		otherB = "! Blocking !";
		otherFuel = "! Fuel Empty !";
		otherFull = "! Full !";
	};
	TelemetryServer.simple.searchInvert = {"sowing", "sowingmachine", "sprayer", "fertilizer", "manure"};
	TelemetryServer.simple.color = {
		default={1, 1, 1, 1}
	};
	
	TelemetryServer:setGetCfg(true);	
end;

function TelemetryServer:deleteMap()
	if g_server ~= nil and g_client ~= nil and g_dedicatedServerInfo ~= nil then return;end;
	TelemetryServer:setGetCfg(false);	
end;

function TelemetryServer:mouseEvent(posX, posY, isDown, isUp, button)
end;

function TelemetryServer:keyEvent(unicode, sym, modifier, isDown)
	if g_server ~= nil and g_client ~= nil and g_dedicatedServerInfo ~= nil then return;end;
	if TelemetryServer.simple.rebootXml and Input.isKeyPressed(Input.KEY_esc) then
		if g_currentMission.inGameMenu.isOpen then TelemetryServer:setGetCfg(true);end;		
	end;	
end;

function TelemetryServer:update(dt)	
end;

function TelemetryServer:draw(dt)
	if g_server ~= nil and g_client ~= nil and g_dedicatedServerInfo ~= nil then 
		return;
	end;
	
	--respect settings for other mods
	setTextAlignment(0);
	setTextColor(1, 1, 1, 1);
	setTextBold(false);
	--respect settings for other mods
	
	if TelemetryServer.simple.isOn then
		TelemetryServer:writeData();
	end;	
end;

addModEventListener(TelemetryServer);

function TelemetryServer:setGetCfg(loadCfg)
	createFolder(getUserProfileAppPath().. "modsSettings/");
	createFolder(TelemetryServer.settingsDir);
	local file = TelemetryServer.settingsDir.. "TelemetryServerSettings.xml";
	local Xml = nil;
	if not fileExists(file) or not loadCfg then
		Xml = createXMLFile("TelemetryServer_XML", file, "TelemetryServerSettings");
		local groupNameTag = ("TelemetryServerSettings.simple.global(%d)"):format(0);		
		setXMLFloat(Xml, groupNameTag.. "#posX", TelemetryServer.simple.defaultPosX);
		setXMLFloat(Xml, groupNameTag.. "#posY", TelemetryServer.simple.defaultPosY);		
		setXMLFloat(Xml, groupNameTag.. "#size", TelemetryServer.simple.defaultSize);	
		setXMLBool(Xml, groupNameTag.. "#allTxtBold", TelemetryServer.simple.allTxtBold);
		setXMLBool(Xml, groupNameTag.. "#showHelpOn", TelemetryServer.simple.showHelpOn);
		setXMLBool(Xml, groupNameTag.. "#showHelpOff", TelemetryServer.simple.showHelpOff);
		setXMLBool(Xml, groupNameTag.. "#rebootXmlESC", TelemetryServer.simple.rebootXml);
		setXMLBool(Xml, groupNameTag.. "#isOn", TelemetryServer.simple.isOn);
		
		groupNameTag = ("TelemetryServerSettings.simple.show(%d)"):format(0);
		setXMLBool(Xml, groupNameTag.. "#showField", TelemetryServer.simple.showField);
		setXMLBool(Xml, groupNameTag.. "#showSpeed", TelemetryServer.simple.showSpeed);		
		setXMLBool(Xml, groupNameTag.. "#showFillName", TelemetryServer.simple.showFillName);
		setXMLBool(Xml, groupNameTag.. "#realFillName", TelemetryServer.simple.realFillName);		
		setXMLBool(Xml, groupNameTag.. "#showFillLevel", TelemetryServer.simple.showFillLevel);
		setXMLBool(Xml, groupNameTag.. "#showProzent", TelemetryServer.simple.showProzent);
		setXMLBool(Xml, groupNameTag.. "#showPlayerName", TelemetryServer.simple.showPlayerName);
		setXMLBool(Xml, groupNameTag.. "#showVehicleName", TelemetryServer.simple.showVehicleName);
		setXMLBool(Xml, groupNameTag.. "#showVehicleSmallName", TelemetryServer.simple.showVehicleSmallName);
		setXMLBool(Xml, groupNameTag.. "#showVehicleTypeName", TelemetryServer.simple.showVehicleTypeName);
		setXMLBool(Xml, groupNameTag.. "#showLastAllAttached", TelemetryServer.simple.showLastAllAttached);		
		
		groupNameTag = ("TelemetryServerSettings.simple.showVehicle(%d)"):format(0);
		setXMLBool(Xml, groupNameTag.. "#isFillFull", TelemetryServer.simple.isFillFull);
		setXMLBool(Xml, groupNameTag.. "#isTrain", TelemetryServer.simple.showTrain);
		setXMLBool(Xml, groupNameTag.. "#isPlayer", TelemetryServer.simple.isPlayer);
		setXMLBool(Xml, groupNameTag.. "#isHelper", TelemetryServer.simple.isHelper);
		
		groupNameTag = ("TelemetryServerSettings.simple.extraShow(%d)"):format(0);
		setXMLBool(Xml, groupNameTag.. "#isFuel", TelemetryServer.simple.isFuel);
		setXMLBool(Xml, groupNameTag.. "#isBlocking", TelemetryServer.simple.isBlocking);
		setXMLBool(Xml, groupNameTag.. "#isDamage", TelemetryServer.simple.isDamage);
		setXMLBool(Xml, groupNameTag.. "#isWater", TelemetryServer.simple.isWater);
		
		groupNameTag = ("TelemetryServerSettings.simple.other(%d)"):format(0);
		--setXMLInt(Xml, groupNameTag.. "#attachedLimit", TelemetryServer.simple.attachedLimit);		
		--setXMLFloat(Xml, groupNameTag.. "#diffPosX", TelemetryServer.simple.diffPosX);
		--setXMLFloat(Xml, groupNameTag.. "#diffPosY", TelemetryServer.simple.diffPosY);
		local searchInvertTemp = ""
		for key,value in pairs(TelemetryServer.simple.searchInvert) do
			if string.len(searchInvertTemp) > 0 then searchInvertTemp = searchInvertTemp.. ",".. tostring(TelemetryServer.simple.searchInvert[key])
			else searchInvertTemp = searchInvertTemp.. tostring(TelemetryServer.simple.searchInvert[key]);end;
		end;
		setXMLString(Xml, groupNameTag.. "#searchInvert", tostring(searchInvertTemp));
		
		groupNameTag = ("TelemetryServerSettings.simple.seperator(%d)"):format(0);
		setXMLString(Xml, groupNameTag.. "#default", TelemetryServer.simple.seperator);
		setXMLString(Xml, groupNameTag.. "#fillNameToFillLevel", TelemetryServer.simple.txt.fillNameToFillLevel);		
		
		groupNameTag = ("TelemetryServerSettings.simple.txt(%d)"):format(0);		
		setXMLString(Xml, groupNameTag.. "#speedTxt", TelemetryServer.simple.txt.speed);
		setXMLString(Xml, groupNameTag.. "#fieldTxt", TelemetryServer.simple.txt.field);
		setXMLString(Xml, groupNameTag.. "#otherT", TelemetryServer.simple.txt.otherT);
		setXMLString(Xml, groupNameTag.. "#helperS", TelemetryServer.simple.txt.helperS);
		setXMLString(Xml, groupNameTag.. "#playerS", TelemetryServer.simple.txt.playerS);
		setXMLString(Xml, groupNameTag.. "#otherSa", TelemetryServer.simple.txt.otherSa);
		setXMLString(Xml, groupNameTag.. "#otherSaFull", TelemetryServer.simple.txt.otherSaFull);
		setXMLString(Xml, groupNameTag.. "#otherSaOrFill", TelemetryServer.simple.txt.otherSaOrFill);
		setXMLString(Xml, groupNameTag.. "#otherW", TelemetryServer.simple.txt.otherW);
		setXMLString(Xml, groupNameTag.. "#otherD", TelemetryServer.simple.txt.otherD);
		setXMLString(Xml, groupNameTag.. "#otherB", TelemetryServer.simple.txt.otherB);
		setXMLString(Xml, groupNameTag.. "#otherFuel", TelemetryServer.simple.txt.otherFuel);
		setXMLString(Xml, groupNameTag.. "#otherFull", TelemetryServer.simple.txt.otherFull);
		
		groupNameTag = ("TelemetryServerSettings.simple.color(%d)"):format(0);
		setXMLFloat(Xml, groupNameTag.. "#defaultColorA", TelemetryServer.simple.color.default[4]);
		setXMLFloat(Xml, groupNameTag.. "#defaultColorB", TelemetryServer.simple.color.default[3]);
		setXMLFloat(Xml, groupNameTag.. "#defaultColorG", TelemetryServer.simple.color.default[2]);
		setXMLFloat(Xml, groupNameTag.. "#defaultColorR", TelemetryServer.simple.color.default[1]);
		saveXMLFile(Xml);
	else
		Xml = loadXMLFile("TelemetryServer_XML", file, "TelemetryServerSettings");
		local groupNameTag = ("TelemetryServerSettings.simple.global(%d)"):format(0);
		if getXMLBool(Xml, groupNameTag.. "#isOn") ~= nil then TelemetryServer.simple.isOn = getXMLBool(Xml, groupNameTag.. "#isOn");end;
		if getXMLBool(Xml, groupNameTag.. "#rebootXmlESC") ~= nil then TelemetryServer.simple.rebootXml = getXMLBool(Xml, groupNameTag.. "#rebootXmlESC");end;
		if getXMLBool(Xml, groupNameTag.. "#showHelpOn") ~= nil then TelemetryServer.simple.showHelpOn = getXMLBool(Xml, groupNameTag.. "#showHelpOn");end;
		if getXMLBool(Xml, groupNameTag.. "#showHelpOff") ~= nil then TelemetryServer.simple.showHelpOff = getXMLBool(Xml, groupNameTag.. "#showHelpOff");end;
		if getXMLBool(Xml, groupNameTag.. "#allTxtBold") ~= nil then TelemetryServer.simple.allTxtBold = getXMLBool(Xml, groupNameTag.. "#allTxtBold");end;
		if getXMLFloat(Xml, groupNameTag.. "#size") then TelemetryServer.simple.defaultSize = getXMLFloat(Xml, groupNameTag.. "#size");end;
		if getXMLFloat(Xml, groupNameTag.. "#posX") then TelemetryServer.simple.defaultPosX = getXMLFloat(Xml, groupNameTag.. "#posX");end;
		if getXMLFloat(Xml, groupNameTag.. "#posY") then TelemetryServer.simple.defaultPosY = getXMLFloat(Xml, groupNameTag.. "#posY");end;
		
		groupNameTag = ("TelemetryServerSettings.simple.show(%d)"):format(0);
		if getXMLBool(Xml, groupNameTag.. "#showField") ~= nil then TelemetryServer.simple.showField = getXMLBool(Xml, groupNameTag.. "#showField");end;
		if getXMLBool(Xml, groupNameTag.. "#showSpeed") ~= nil then TelemetryServer.simple.showSpeed = getXMLBool(Xml, groupNameTag.. "#showSpeed");end;
		if getXMLBool(Xml, groupNameTag.. "#showFillName") ~= nil then TelemetryServer.simple.showFillName = getXMLBool(Xml, groupNameTag.. "#showFillName");end;
		if getXMLBool(Xml, groupNameTag.. "#showFillLevel") ~= nil then TelemetryServer.simple.showFillLevel = getXMLBool(Xml, groupNameTag.. "#showFillLevel");end;
		if getXMLBool(Xml, groupNameTag.. "#realFillName") ~= nil then TelemetryServer.simple.realFillName = getXMLBool(Xml, groupNameTag.. "#realFillName");end;
		if getXMLBool(Xml, groupNameTag.. "#showProzent") ~= nil then TelemetryServer.simple.showProzent = getXMLBool(Xml, groupNameTag.. "#showProzent");end;
		if getXMLBool(Xml, groupNameTag.. "#showPlayerName") ~= nil then TelemetryServer.simple.showPlayerName = getXMLBool(Xml, groupNameTag.. "#showPlayerName");end;
		if getXMLBool(Xml, groupNameTag.. "#showVehicleName") ~= nil then TelemetryServer.simple.showVehicleName = getXMLBool(Xml, groupNameTag.. "#showVehicleName");end;
		if getXMLBool(Xml, groupNameTag.. "#showVehicleSmallName") ~= nil then TelemetryServer.simple.showVehicleSmallName = getXMLBool(Xml, groupNameTag.. "#showVehicleSmallName");end;
		if getXMLBool(Xml, groupNameTag.. "#showVehicleTypeName") ~= nil then TelemetryServer.simple.showVehicleTypeName = getXMLBool(Xml, groupNameTag.. "#showVehicleTypeName");end;
		if getXMLBool(Xml, groupNameTag.. "#showLastAllAttached") ~= nil then TelemetryServer.simple.showLastAllAttached = getXMLBool(Xml, groupNameTag.. "#showLastAllAttached");end;
		
		groupNameTag = ("TelemetryServerSettings.simple.showVehicle(%d)"):format(0);
		if getXMLBool(Xml, groupNameTag.. "#isFillFull") ~= nil then TelemetryServer.simple.isFillFull = getXMLBool(Xml, groupNameTag.. "#isFillFull");end;
		if getXMLBool(Xml, groupNameTag.. "#isTrain") ~= nil then TelemetryServer.simple.isTrain = getXMLBool(Xml, groupNameTag.. "#isTrain");end;
		if getXMLBool(Xml, groupNameTag.. "#isPlayer") ~= nil then TelemetryServer.simple.isPlayer = getXMLBool(Xml, groupNameTag.. "#isPlayer");end;
		if getXMLBool(Xml, groupNameTag.. "#isHelper") ~= nil then TelemetryServer.simple.isHelper = getXMLBool(Xml, groupNameTag.. "#isHelper");end;		
		
		groupNameTag = ("TelemetryServerSettings.simple.extraShow(%d)"):format(0);
		if getXMLBool(Xml, groupNameTag.. "#isBlocking") ~= nil then TelemetryServer.simple.isBlocking = getXMLBool(Xml, groupNameTag.. "#isBlocking");end;
		if getXMLBool(Xml, groupNameTag.. "#isDamage") ~= nil then TelemetryServer.simple.isDamage = getXMLBool(Xml, groupNameTag.. "#isDamage");end;
		if getXMLBool(Xml, groupNameTag.. "#isWater") ~= nil then TelemetryServer.simple.isWater = getXMLBool(Xml, groupNameTag.. "#isWater");end;
		if getXMLBool(Xml, groupNameTag.. "#isFuel") ~= nil then TelemetryServer.simple.isFuel = getXMLBool(Xml, groupNameTag.. "#isFuel");end;
		
		groupNameTag = ("TelemetryServerSettings.simple.other(%d)"):format(0);
		if getXMLFloat(Xml, groupNameTag.. "#diffPosX") then TelemetryServer.simple.diffPosX = getXMLFloat(Xml, groupNameTag.. "#diffPosX");end;
		if getXMLFloat(Xml, groupNameTag.. "#diffPosY") then TelemetryServer.simple.diffPosY = getXMLFloat(Xml, groupNameTag.. "#diffPosY");end;
		if getXMLInt(Xml, groupNameTag.. "#attachedLimit") then TelemetryServer.simple.attachedLimit = getXMLInt(Xml, groupNameTag.. "#attachedLimit");end;
		local searchInvertTemp = "";
		if getXMLString(Xml, groupNameTag.. "#searchInvert") ~= nil then searchInvertTemp = getXMLString(Xml, groupNameTag.. "#searchInvert");end;
		local stringSplit = TelemetryServer:stringSplit(searchInvertTemp, ",");
		if stringSplit ~= nil and #stringSplit > 0 then
			searchInvertTemp = ",";
			--for a=1, #stringSplit do
			--	if stringSplit[a] ~= nil then
			--		local invertString = tostring(stringSplit[a]);
			--		searchInvertTemp = searchInvertTemp.. invertString.. ",";
			--		if TelemetryServer.simple.searchInvert[invertString] == nil then table.insert(TelemetryServer.simple.searchInvert, invertString);end;
			--	end;
			--end;
			TelemetryServer.simple.searchInvert = {};
			for key,value in pairs(stringSplit) do				
				if TelemetryServer.simple.searchInvert[key] == nil then table.insert(TelemetryServer.simple.searchInvert, tostring(string.lower(stringSplit[key])));end;
				searchInvertTemp = searchInvertTemp.. tostring(string.lower(stringSplit[key])).. ",";
			end;
			--for key,value in pairs(TelemetryServer.simple.searchInvert) do
			--	if not string.find(searchInvertTemp, ",".. TelemetryServer.simple.searchInvert[key].. ",") then
			--		TelemetryServer.simple.searchInvert[key] = nil;
			--	end;
			--end;
		end;
		
		groupNameTag = ("TelemetryServerSettings.simple.seperator(%d)"):format(0);
		if getXMLString(Xml, groupNameTag.. "#default") ~= nil then TelemetryServer.simple.seperator = getXMLString(Xml, groupNameTag.. "#default");end;
		if getXMLString(Xml, groupNameTag.. "#fillNameToFillLevel") ~= nil then TelemetryServer.simple.txt.fillNameToFillLevel = getXMLString(Xml, groupNameTag.. "#fillNameToFillLevel");end;
		
		groupNameTag = ("TelemetryServerSettings.simple.txt(%d)"):format(0);
		if getXMLString(Xml, groupNameTag.. "#speedTxt") ~= nil then TelemetryServer.simple.txt.speed = getXMLString(Xml, groupNameTag.. "#speedTxt");end;
		if getXMLString(Xml, groupNameTag.. "#fieldTxt") ~= nil then TelemetryServer.simple.txt.field = getXMLString(Xml, groupNameTag.. "#fieldTxt");end;
		if getXMLString(Xml, groupNameTag.. "#otherT") ~= nil then TelemetryServer.simple.txt.otherT = getXMLString(Xml, groupNameTag.. "#otherT");end;
		if getXMLString(Xml, groupNameTag.. "#otherSa") ~= nil then TelemetryServer.simple.txt.otherSa = getXMLString(Xml, groupNameTag.. "#otherSa");end;
		if getXMLString(Xml, groupNameTag.. "#otherSaFull") ~= nil then TelemetryServer.simple.txt.otherSaFull = getXMLString(Xml, groupNameTag.. "#otherSaFull");end;
		if getXMLString(Xml, groupNameTag.. "#otherSaOrFill") ~= nil then TelemetryServer.simple.txt.otherSaOrFill = getXMLString(Xml, groupNameTag.. "#otherSaOrFill");end;
		if getXMLString(Xml, groupNameTag.. "#otherW") ~= nil then TelemetryServer.simple.txt.otherW = getXMLString(Xml, groupNameTag.. "#otherW");end;
		if getXMLString(Xml, groupNameTag.. "#otherD") ~= nil then TelemetryServer.simple.txt.otherD = getXMLString(Xml, groupNameTag.. "#otherD");end;
		if getXMLString(Xml, groupNameTag.. "#otherB") ~= nil then TelemetryServer.simple.txt.otherB = getXMLString(Xml, groupNameTag.. "#otherB");end;
		if getXMLString(Xml, groupNameTag.. "#otherFuel") ~= nil then TelemetryServer.simple.txt.otherFuel = getXMLString(Xml, groupNameTag.. "#otherFuel");end;
		if getXMLString(Xml, groupNameTag.. "#otherFull") ~= nil then TelemetryServer.simple.txt.otherFull = getXMLString(Xml, groupNameTag.. "#otherFull");end;
		if getXMLString(Xml, groupNameTag.. "#helperS") ~= nil then TelemetryServer.simple.txt.helperS = getXMLString(Xml, groupNameTag.. "#helperS");end;
		if getXMLString(Xml, groupNameTag.. "#playerS") ~= nil then TelemetryServer.simple.txt.playerS = getXMLString(Xml, groupNameTag.. "#playerS");end;
		
		groupNameTag = ("TelemetryServerSettings.simple.color(%d)"):format(0);
	end;
end;

function TelemetryServer:setPosX_Y(showHelpOn, showHelpOff, isShowHelpMenu)
	if showHelpOn and isShowHelpMenu then
		if g_currentMission.hud.inputHelp ~= nil and g_currentMission.hud.inputHelp.overlay ~= nil then
			TelemetryServer.simple.defaultPosY = g_currentMission.hud.inputHelp.overlay.y;
		end;
	elseif showHelpOff and not isShowHelpMenu then
		if (g_currentMission.vehicleFruitHud ~= nil and g_currentMission.vehicleFruitHud.schemaPos == 1) or g_currentMission.vehicleFruitHud == nil then
			TelemetryServer.simple.defaultPosY = TelemetryServer.simple.vehicleSchemaPosY;
		end;
	end;
end;


function TelemetryServer:getStorages()	
	for key,storage in pairs(TelemetryServer.storages) do
		local storageObject = storage.object.storages[1];		
		if storageObject ~= nil then		
			for fillType,isAccepted in pairs(storageObject.fillTypes) do
				if isAccepted and TelemetryServer.fillTypes[fillType] ~= nil then
					TelemetryServer.fillTypes[fillType].amount = Utils.getNoNil(storageObject.fillLevels[fillType]);
					if drawAmountFillType[fillType] == nil then drawAmountFillType[fillType] = {amount=0};end;
					drawAmountFillType[fillType].amount = drawAmountFillType[fillType].amount+Utils.getNoNil(storageObject.fillLevels[fillType], 0);
				end;
			end;
		end;
	end;
end;


function TelemetryServer:loadFillTypes()
	TelemetryServer.fillTypes = {};	
	local notFillTypes = {"horse_type", "cow_type", "sheep_type", "chicken_type", "pig_type", "unknown", "air", "def", "tarp", "treesaplings", "diesel", "fuel", "poplar", "weed"};
	local notFillNames = {"grass_windrow", "drygrass"};
	function isFill(descName)
		for _,notFill in pairs(notFillTypes) do			
			if string.find(descName, "grass") and isFillNames(descName) then return false;end;
			if string.find(descName, notFill) then return false;end;
		end;
		return true;
	end;
	function isFillNames(descName)
		local foundExactName = false;
		for _,notFill in pairs(notFillNames) do			
			if string.len(descName) == string.len(notFill) then foundExactName = true;end;
		end;
		return foundExactName;
	end;
	for _,desc in pairs(g_currentMission.fillTypeManager.fillTypes) do		
		if desc ~= nil and desc.name ~= nil then
			local descName = tostring(string.lower(desc.name));						
			if isFill(descName) then
				TelemetryServer.fillTypes[desc.index] = {};
				TelemetryServer.fillTypes[desc.index].fruitName = desc.title;				
				TelemetryServer.fillTypes[desc.index].descName = desc.name;				
				TelemetryServer.fillTypes[desc.index].descIndex = desc.index;
				TelemetryServer.fillTypes[desc.index].bestPrice = 0;
				TelemetryServer.fillTypes[desc.index].bestPriceStation = "";
				--TelemetryServer.fillTypes[desc.index].greatDemandStation = nil;
				--TelemetryServer.fillTypes[desc.index].greatDemandMultiplier = 1;
				TelemetryServer.fillTypes[desc.index].showOnPriceTable = desc.showOnPriceTable;
				TelemetryServer.fillTypes[desc.index].pricePerLiter = Utils.getNoNil(desc.pricePerLiter, 0);
				TelemetryServer.fillTypes[desc.index].previousHourPrice = Utils.getNoNil(desc.previousHourPrice, 0);				
				TelemetryServer.fillTypes[desc.index].massPerLiter = Utils.getNoNil(desc.massPerLiter, 0);
				TelemetryServer.fillTypes[desc.index].startPricePerLiter = Utils.getNoNil(desc.startPricePerLiter, 0);
				TelemetryServer.fillTypes[desc.index].totalAmount = Utils.getNoNil(desc.totalAmount, 0);
				TelemetryServer.fillTypes[desc.index].amount = 0;
			end;
		end;
	end;
end;


function TelemetryServer:loadPlaceables()
	TelemetryServer.storages = {}; --check extension storage later
	TelemetryServer.sellStations = {}; --check extension storage later

	for a=1, #g_currentMission.placeables do
		if g_currentMission.placeables[a] ~= nil then
			local object = g_currentMission.placeables[a];
			if object ~= nil and object.nodeId ~= nil then
				if object.storages ~= nil and type(object.storages) == "table" and object.loadingStation ~= nil and object.loadingStation.stationName ~= nil then
					TelemetryServer.storages[#TelemetryServer.storages+1] = {};
					TelemetryServer.storages[#TelemetryServer.storages].posOnTable = #TelemetryServer.storages;
					TelemetryServer.storages[#TelemetryServer.storages].nodeId = object.nodeId;
					TelemetryServer.storages[#TelemetryServer.storages].object = object;
					TelemetryServer.storages[#TelemetryServer.storages].name = Utils.getNoNil(object.loadingStation.stationName, "Unknown");
				elseif object.sellingStation ~= nil and type(object.sellingStation) == "table" and object.sellingStation.incomeName ~= nil then					
					if string.find(object.sellingStation.incomeName, "harvestIncome") then						
						TelemetryServer.sellStations[#TelemetryServer.sellStations+1] = {};
						TelemetryServer.sellStations[#TelemetryServer.sellStations].object = object;
						TelemetryServer.sellStations[#TelemetryServer.sellStations].posOnTable = #TelemetryServer.sellStations;
						TelemetryServer.sellStations[#TelemetryServer.sellStations].nodeId = object.nodeId;
						TelemetryServer.sellStations[#TelemetryServer.sellStations].name = Utils.getNoNil(object.sellingStation.stationName, "Unknown");
						TelemetryServer.sellStations[#TelemetryServer.sellStations].appearsOnStats = Utils.getNoNil(object.sellingStation.appearsOnStats, false);						
					end;
				end;
			end;
		end;
	end;
end;

local nOutputTicks = 60;
function TelemetryServer:writeData()	
	
	-- Write XML if counter on top
	if(nOutputTicksCurrent >= nOutputTicks) then
		if fileExists(dataFile) then	
			dataXML = loadXMLFile("TelemetryData_XML", dataFile, "TelemetryData");
		--	print("loadXMLFile")
		else							
			dataXML = createXMLFile("TelemetryData_XML", dataFile, "TelemetryData");
		--	print("createXMLFile")
		end;
		nOutputTicksCurrent = 0;
	else
		dataXML = nil;		
		nOutputTicksCurrent = nOutputTicksCurrent+1
		return;
	end

	
	drawAmountFillType = {};
	--get stations	
	TelemetryServer:loadPlaceables();
	TelemetryServer:loadFillTypes();
	TelemetryServer:getStorages();


	
	for key, fillType in pairs(TelemetryServer.fillTypes) do
		local xmlKey = "TelemetryData.FillType_".. tostring(key)

		setXMLString(dataXML, xmlKey.. ".Title", fillType.fruitName);
		setXMLString(dataXML, xmlKey.. ".Name", fillType.descName);
		setXMLInt(dataXML, xmlKey.. ".ID", fillType.descIndex);
	--	setXMLInt(dataXML, xmlKey.. ".bestPrice", fillType.bestPrice);
	--	setXMLString(dataXML, xmlKey.. ".bestPriceStation", fillType.bestPriceStation);
	--	setXMLBool(dataXML, xmlKey.. ".showOnPriceTable", fillType.showOnPriceTable);
	--	setXMLInt(dataXML, xmlKey.. ".pricePerLiter", fillType.pricePerLiter);
	--	setXMLInt(dataXML, xmlKey.. ".previousHourPrice", fillType.previousHourPrice);
	--	setXMLInt(dataXML, xmlKey.. ".massPerLiter", fillType.massPerLiter);
--		setXMLInt(dataXML, xmlKey.. ".startPricePerLiter", fillType.startPricePerLiter);
	--	setXMLInt(dataXML, xmlKey.. ".totalAmount", fillType.totalAmount);
	--	setXMLInt(dataXML, xmlKey.. ".amount", fillType.amount);

		for key2, station in pairs(TelemetryServer.sellStations) do
			local xmlKey = "TelemetryData.FillType_".. tostring(key) .. ".Prices_".. tostring(key2)
			local sellingStation = TelemetryServer.sellStations[station.posOnTable].object.sellingStation;
			if sellingStation ~= nil then
				if TelemetryServer:StationAcceptsType(sellingStation, fillType.descIndex) then
								
					local pricePerLiter = (
						(sellingStation.fillTypePrices[fillType.descIndex] + sellingStation.fillTypePriceRandomDelta[fillType.descIndex])
						* sellingStation.priceMultipliers[fillType.descIndex] * EconomyManager.getPriceMultiplier())
						*1000;

					local fillTypePrice = g_i18n:formatMoney(pricePerLiter);
			
					setXMLString(dataXML, xmlKey.. ".Title", tostring(station.name));
					setXMLString(dataXML, xmlKey.. ".ID", tostring(station.nodeId));
					setXMLString(dataXML, xmlKey.. ".PriceF", fillTypePrice);
					setXMLInt(dataXML, xmlKey.. ".Price", pricePerLiter);
				end;
			end;
		end;
	end;



	for key,station in pairs(TelemetryServer.sellStations) do
		if station.appearsOnStats then
			local stationName = tostring(station.name);

			local xmlIdx = 0;
			local sellingStation = TelemetryServer.sellStations[station.posOnTable].object.sellingStation;
			if sellingStation ~= nil then

				local xmlKey = "TelemetryData.Station_".. tostring(xmlIdx)
				xmlIdx = xmlIdx+ 1;

				for fillType, isAccepted in pairs(sellingStation.acceptedFillTypes) do
					if isAccepted and TelemetryServer.fillTypes[fillType] ~= nil then									
						local pricePerLiter = ((sellingStation.fillTypePrices[fillType]+sellingStation.fillTypePriceRandomDelta[fillType])*sellingStation.priceMultipliers[fillType]*EconomyManager.getPriceMultiplier())*1000;
						local fillTypePrice = g_i18n:formatMoney(pricePerLiter);	

						setXMLString(dataXML, xmlKey.. ".FillType_" .. TelemetryServer.fillTypes[fillType].fruitName .. ".PerLiter", fillTypePrice);	
					else			
					--	print("!isAccepted " .. tostring(station.name))												
					end;
				end;
			else			
			--	print("sellingStation ~= nil " .. tostring(station.name))
			end;
		else			
		--	print("ELSE 1 " .. tostring(station.name))
		end;
	end;


	--get stations
	
	if g_currentMission.vehicles ~= nil then
			
		--for key, value in pairs(g_currentMission.vehicles) do
		for a=1, #g_currentMission.vehicles do
			local vehicle = g_currentMission.vehicles[a];			
			if vehicle ~= nil and vehicle.getIsControlled ~= nil then --and vehicle.getCurrentHelper ~= nil then
				local txtWidth = 0;
				local seperator = TelemetryServer.simple.seperator;
				local fillNameToFillLevel = TelemetryServer.simple.txt.fillNameToFillLevel;
				local helperS = TelemetryServer.simple.txt.helperS;
				local otherT = TelemetryServer.simple.txt.otherT;			
				local playerS = TelemetryServer.simple.txt.playerS;				
				local otherSaFull = TelemetryServer.simple.txt.otherSaFull;
				local otherSaOrFill = TelemetryServer.simple.txt.otherSaOrFill;
				local otherSa = TelemetryServer.simple.txt.otherSa;
				local otherW = TelemetryServer.simple.txt.otherW;
				local otherD = TelemetryServer.simple.txt.otherD;
				local otherB = TelemetryServer.simple.txt.otherB;
				local otherFuel = TelemetryServer.simple.txt.otherFuel;
				local otherFull = TelemetryServer.simple.txt.otherFull;
				local outputString = {};

				local xmlKey = "TelemetryData.Vehicles.Vehicle_".. tostring(a)

				--print("Printing " .. xmlKey)
				--TelemetryServer:dmp(vehicle, 0);


				setXMLBool(dataXML, xmlKey .. ".IsActive", vehicle.getIsControlled(vehicle) and g_gameSettings.nickname == vehicle.getControllerName(vehicle));

				local showLine = true;

				setXMLBool(dataXML, xmlKey .. ".IsTrain", ((vehicle.viVehicleName ~= nil and vehicle.viVehicleName == "Train") or vehicle.trainSystem ~= nil));

				if vehicle.getCurrentHelper ~= nil and vehicle.getCurrentHelper(vehicle) then 
					setXMLBool(dataXML, xmlKey .. ".HelperActive", true);
				else
					setXMLBool(dataXML, xmlKey .. ".HelperActive", false);
				end;

				-- show players name on active vehicle
				if vehicle.getIsControlled(vehicle) then 
				   setXMLString(dataXML, xmlKey .. ".Driver", vehicle.getControllerName(vehicle));
				else
					setXMLString(dataXML, xmlKey .. ".Driver", "");
				end;
				
				setXMLString(dataXML, xmlKey .. ".viVehicleName", tostring(vehicle.viVehicleName));
				setXMLString(dataXML, xmlKey .. ".Name", tostring(vehicle.getName(vehicle)));
				setXMLString(dataXML, xmlKey .. ".Desc", tostring(vehicle.typeDesc));

				--Needs to be calculated to Ingame Values
				if(vehicle.mapHotspot ~= nil) then
					setXMLFloat(dataXML, xmlKey .. ".Pos.Z", vehicle.mapHotspot.zMapPos);
					setXMLFloat(dataXML, xmlKey .. ".Pos.X", vehicle.mapHotspot.xMapPos);
				--	setXMLFloat(dataXML, xmlKey .. ".Pos.Y", vehicle.mapHotspot.yMapPos);
				end;
				setXMLInt(dataXML, xmlKey .. ".CruiseControl.Status", vehicle.spec_drivable.cruiseControl.state);
				setXMLInt(dataXML, xmlKey .. ".CruiseControl.Speed", vehicle.spec_drivable.cruiseControl.speed);
				setXMLInt(dataXML, xmlKey .. ".CruiseControl.SpeedSent", vehicle.spec_drivable.cruiseControl.speedSent);
				setXMLBool(dataXML, xmlKey .. ".CruiseControl.IsActive", vehicle.spec_drivable.cruiseControl.isActive);
				

				if vehicle.spec_motorized ~= nil then
					setXMLString(dataXML, xmlKey .. ".MotorStarted", tostring(vehicle.spec_motorized.isMotorStarted));
				end;
					
				setXMLBool(dataXML, xmlKey .. ".OnField", vehicle.getIsOnField(vehicle));
				setXMLInt(dataXML, xmlKey .. ".Field", TelemetryServer:getIsOnField(vehicle));					
				setXMLString(dataXML, xmlKey .. ".Speed", TelemetryServer:getSpeed(vehicle));					
				setXMLBool(dataXML, xmlKey .. ".InWater", vehicle.showTailwaterDepthWarning);	
				setXMLBool(dataXML, xmlKey .. ".Damaged", vehicle.isBroken);			
				
				
				if TelemetryServer.simple.isBlocking and not vehicle.showTailwaterDepthWarning and vehicle.spec_aiVehicle ~= nil and vehicle.spec_aiVehicle.debugTexts ~= nil then							
					if vehicle.spec_aiVehicle.debugTexts[1] ~= nil and string.find(vehicle.spec_aiVehicle.debugTexts[1], "STOP") and string.find(vehicle.spec_aiVehicle.debugTexts[1], "collision") then		
						setXMLBool(dataXML, xmlKey .. ".Blocked", true);
					else
						setXMLBool(dataXML, xmlKey .. ".Blocked", false);
					end;						
				end;
									
				local attach, attachedSprayerAnd = TelemetryServer:getAttachedAndSprayerAnd(vehicle, 1);
				local fillLevelTable = {};
				local FillCount = 0;
				if vehicle.getFillLevelInformation ~= nil then
					vehicle:getFillLevelInformation(fillLevelTable);
					for idx ,fillLevelVehicle in pairs(fillLevelTable) do
						if fillLevelVehicle.fillLevel > 0 then
							local fillKey = xmlKey .. ".Fill_" .. tostring(idx) .."."

							local typeName = Utils.getNoNil(string.lower(vehicle.typeName), "Unknown");
							local invertProzent = TelemetryServer:isInvertFill(typeName) or (attachedSprayerAnd > 0 and attach == 1) or (attachedSprayerAnd == attach);--string.find(typeName, "sowing") or string.find(typeName, "sprayer") or string.find(typeName, "manure") or string.find(typeName, "fertilizer") or attachedSprayerAnd > 0;
							
							setXMLFloat(dataXML, fillKey.."Level" , Utils.getNoNil(fillLevelVehicle.fillLevel, 0));
							setXMLInt(dataXML, fillKey.."Capacity" , fillLevelVehicle.capacity);
							setXMLInt(dataXML, fillKey.."Percent" , TelemetryServer:getProzentString(fillLevelVehicle.fillLevel, fillLevelVehicle.capacity, true));
							
							
							local fillTypeName = FillTypeManager.getFillTypeNameByIndex(g_fillTypeManager, fillLevelVehicle.fillType);

							setXMLBool(dataXML, fillKey.."Inverted" , invertProzent);
							setXMLString(dataXML, fillKey.."Title" , g_fillTypeManager.nameToFillType[fillTypeName].title);
							setXMLString(dataXML, fillKey.."Name" , FillTypeManager.getFillTypeNameByIndex(g_fillTypeManager, fillLevelVehicle.fillType));
							setXMLInt(dataXML, fillKey.."ID" , fillLevelVehicle.fillType);
						end;
					end;						
				end;
				setXMLInt(dataXML, xmlKey .. ".Attachments", tonumber(attach));			
				
			end;
		end;
	end;	

	saveXMLFile(dataXML);
end;


local nDumpLevel = 0
local nindentMax = 3

function TelemetryServer:dmp(tbl, indent)


	if not indent then 
		indent = 0 
	end; 
	
	if indent < 3 then
		for k, v in pairs(tbl) do
		
			if type(k) == "table" then
			 -- TelemetryServer:dmp(k, indent+1)
			else
				formatting = string.rep("|  ", indent) .. k .. ": "
				if type(v) == "table" then
				  print(formatting)
				  TelemetryServer:dmp(v, indent+1)
				elseif type(v) == 'boolean' then
				  print(formatting .. tostring(v))  
				elseif type(v) == 'function' then
				--  print(formatting .. tostring(v))      
				else
				  print(formatting .. v)
				end
			end
		end
	end
end;

function TelemetryServer:StationAcceptsType(station, fillType)

	for type, isAccepted in pairs(station.acceptedFillTypes) do
		if type == fillType then									
			return true;									
		end;
	end;

--	print("Not Accepted:  ".. tostring(fillType) .. " at ".. tostring(station.name))
	return false;	
end;

function TelemetryServer:getFullSize()
	if g_currentMission.paused or
	g_currentMission.inGameMenu.paused or	
	g_currentMission.inGameMenu.isOpen or 
	g_currentMission.physicsPaused then
	if g_currentMission.missionDynamicInfo.isMultiplayer and g_currentMission.manualPaused then return false;end;
	return true;end;
	return false;
end;

function TelemetryServer:getIngameMap()
	if g_gameSettings.ingameMapState == 1 then 
		return true;
	end;
	return false;
end;

function TelemetryServer:isInvertFill(typeName)
	for key,value in pairs(TelemetryServer.simple.searchInvert) do
		--print("typeName...".. tostring(typeName).. " - invertName...".. tostring(TelemetryServer.simple.searchInvert[key]))
		if string.find(TelemetryServer.simple.searchInvert[key], typeName) and string.len(typeName) == string.len(TelemetryServer.simple.searchInvert[key]) then return true;end;		
	end;
	return false;
end;

function TelemetryServer:getProzentString(fill, capacity, aNumber)	
	if not aNumber then
		return string.format("%1.0f", "".. tonumber(fill)/tonumber(capacity)*100);
	else
		return fill/capacity*100;
	end;
end;

function TelemetryServer:getSpeed(vehicle)
	local speed = Utils.getNoNil(vehicle.lastSpeed, 0)*3600; --lastSpeed,speedDisplayDt	
	if g_gameSettings:getValue('useMiles') then 
		speed = Utils.getNoNil(vehicle.lastSpeed, 0)*0.62;
	end;
	if vehicle.viVehicleName == "Train" or vehicle.trainSystem ~= nil then 
		speed = Utils.getNoNil(vehicle.lastSpeed, 0)*3.6;
	end;
	return string.format("%1.0f", "".. Utils.getNoNil(speed, 0));
end;

function TelemetryServer:getIsOnField(vehicle)
    local wx,wy,wz = getWorldTranslation(vehicle.components[1].node);
    local densityBits = getDensityAtWorldPos(g_currentMission.terrainDetailId, wx, wy, wz);
    return densityBits, densityBits ~= 0;
end;

function TelemetryServer:getAttachedAndSprayerAnd(vehicle, attachedLimit)	
	local attachTable = vehicle.getAttachedImplements(vehicle)
	local attached = 0;
	local attachedSprayerAnd = 0;
	if attachTable ~= nil and attachedLimit <= TelemetryServer.simple.attachedLimit then		
		for _,attach in pairs(attachTable) do		
			if attach.object ~= nil and attach.object.typeName ~= nil then
				local typeName = Utils.getNoNil(string.lower(attach.object.typeName), "Unknown");
				if not string.find(typeName, "implements") and not string.find(typeName, "Unknown") then
					attached = attached+1;
				end;
				TelemetryServer:isInvertFill(typeName)
				if TelemetryServer:isInvertFill(typeName) then --string.find(typeName, "sowing") or string.find(typeName, "sprayer") or string.find(typeName, "manure") or string.find(typeName, "fertilizer") then
					attachedSprayerAnd = attachedSprayerAnd+1;
				end;
				local tempAttached, tempAttachedSprayerAnd = TelemetryServer:getAttachedAndSprayerAnd(attach.object, attachedLimit+1);
				attached = attached+tempAttached;
				attachedSprayerAnd = attachedSprayerAnd+tempAttachedSprayerAnd;
			end;
		end;
	end;
	return attached, attachedSprayerAnd;
end;

--VI LS17 Script
function TelemetryServer:setVehicleName(self, vehicle, attachable)
	local vehicleName = "Unknown";
	local category = "Unknown";
	function setAlternativeName()
		if vehicle.baleWidth ~= nil then
			vehicle.viVehicleName = "Bale";
			vehicleName = "Bale";
		elseif vehicle.fillablePalletDirtyFlag ~= nil then
			vehicle.viVehicleName = "Pal.";
			vehicleName = "Pal.";
		end;
	end;
	--vehicle.getFullName(vehicle) okay return New Holland Tx 32
	--vehicle.getName(vehicle) okay return Tx32
	--!! not all vehicle.fullViewName = Combine
	--controlGroupNames --a table
	--vehicle.getControllerName(vehicle) okay return your User name
	--className = Vehicle
	--name = combineDrivable
	--nicknameRendering --a table
	--vehicle.typeName = 
	--vehicle.typeDesc = 
	
	--g_currentMission.vehicles.getIsVehicleControlledByPlayer()
	--getIsControlled()
	--getControllerName()
	--getFillUnitFillLevelPercentage()
	--getFillLevelInformation()
	
	if vehicle.configFileName == nil then 
		setAlternativeName();
		vehicle.viVehicleName = vehicleName;
		return;
	end;
	if vehicle.viVehicleName ~= nil then return;end;
	--local storeItem = StoreItemsUtil.storeItemsByXMLFilename[Utils.getNoNil(vehicle.configFileName, vehicle.xmlFilename):lower()];
    --if storeItem ~= nil then
    --    if storeItem.name ~= nil then
    --        vehicleName = tostring(storeItem.name);
    --    end;
        --if storeItem.category ~= nil and storeItem.category ~= "" then
        --    category = tostring(storeItem.category);
        --end;	
	--end;		
	
	vehicleName = tostring(vehicle.getFullName(vehicle));
	
	--local vehicleXml = loadXMLFile("MultiOverlayV2_XML", vehicle.configFileName, "vehicle");
	if vehicle.configFileName ~= nil then
		if string.find(tostring(vehicle.configFileName), "locomotive") then
			vehicleName = "Train";		
			if not attachable then vehicle.typeDesc = vehicleName;end;
		elseif string.find(tostring(vehicle.configFileName), "stationCrane") then
			vehicleName = "Crane";
			if not attachable then vehicle.typeDesc = vehicleName;end;
		elseif string.find(tostring(vehicle.configFileName), "wagon") then
			vehicleName = "WagonFill";
			if string.find(tostring(vehicle.configFileName:lower()), "timber") then
				vehicleName = "WagonTimber";
			end;
			if attachable then vehicle.typeDesc = vehicleName;end;
		elseif string.find(tostring(vehicle.configFileName), "ka26") then
			vehicleName = "Heli";
			if not attachable then vehicle.typeDesc = vehicleName;end;
		elseif vehicle.time ~= nil and vehicle.time > 0 then
			vehicleName = vehicleName.. "-Missions Vehicle";
		end;
	end;
	if vehicleName == "Unknown" then setAlternativeName();end;
	--tableVehicle.name = vehicleName;	
	vehicle.viVehicleName = vehicleName;
end;

function TelemetryServer:stringSplit(txtString, delimiter)
	local result = {};
	local from  = 1;
	local delim_from, delim_to = string.find( txtString, delimiter, from  );
	while delim_from do
		table.insert( result, string.sub( txtString, from , delim_from-1 ) );
		from  = delim_to + 1;
		delim_from, delim_to = string.find( txtString, delimiter, from  );
	end;
	table.insert( result, string.sub( txtString, from  ) );
	return result;
end;